#!/usr/bin/env python
# coding: utf-8
#
# eat - encryption and archive toolkit
#
# author: klosure
# https://gitlab.com/klosure/eat
# license: ISC

import argparse
import errno
import sys
import subprocess
import json
import os
import threading
from os.path import exists, isdir, isfile
from multiprocessing import cpu_count
from os.path import expanduser
from shutil import which

class InvalidConfigFileException(Exception):
    def __init__(self):
        self.message = "Invalid configuration option provided in your .eatrc file"

    def __str__(self):
        return self.message


class EatConfig:

    VERSION = "0.1"
    DESCRIPTION = "eat {0} - encryption and archive toolkit".format(VERSION)

    def __init__(self):
        self.dep_checker = self.DepChecker()
        self.env_setup = self.EnvSetup()

    class DepChecker:
        def __init__(self):
            self.dependencies = ["argon2", "gpg", "par2", "tar", "zstd"]
            self._check()

        def _check(self):
            for dependency in self.dependencies:
                if which(dependency) is None:
                    raise InvalidConfigFileException()

    class EnvSetup:
        def __init__(self):
            self.default_rc_settings = {
                "compression_enabled": True,
                "compression_engine": "zstd",
                "compression_level": "--adapt=min=4,max=18",
                "encryption_enabled": True,
                "cipher": "AES256",
                "pass_pipe": "/tmp/eat_pass_pipe",
                "threads": "-T{0}".format(self._check_system_cores()),
                "quiet_mode": False
            }
            self.home_dir = expanduser("~")
            self.eat_rc = "{0}/.eatrc".format(self.home_dir)
            if not self._check_for_rc_file(self.eat_rc):
                self._create_rc_file(self.eat_rc)
            self.read_rc = self._read_rc_file(self.eat_rc)

        def _check_for_rc_file(self, eat_rc):
            return isfile(eat_rc)

        def _create_rc_file(self, eat_rc):
            with open(eat_rc, 'w') as settings_file:
                settings_file.write(json.dumps(self.default_rc_settings))

        def _check_system_cores(self):
            cpus = cpu_count()
            if cpus > 1:
                cpus = cpus - 1
            return cpus

        def _read_rc_file(self, eat_rc) -> dict:
            open_rc_file = open(eat_rc)
            rc_dict = json.load(open_rc_file)
            open_rc_file.close()
            return rc_dict


class CliInterface:
    def __init__(self, eat_config: EatConfig):
        self.eat_config = eat_config

    def print_version(self):
        print(self.eat_config.DESCRIPTION)

    def print_tar_and_compress_msg(self, new_target_name):
        print("Tar and compressing directory: {0}".format(new_target_name))

    def print_tar_and_compress_encrypt_msg(self, new_target_name):
        print("Tar, compressing, and encrypting directory: {0}".format(new_target_name))

    def print_compress_msg(self, new_target_name):
        print("Compressing file: {0}".format(new_target_name))

    def print_encryption_message(self, new_target_name):
        print("Encrypting file: {0}".format(new_target_name))

    def print_decryption_message(self, target_name):
        print("Decrypting file: {0}".format(target_name))

    def print_par_msg(self, target_name):
        print("Creating par files for: {0}".format(target_name))

    def print_tar_par_msg(self, tar_par_file):
        print("Tar all par files together into: {0}".format(tar_par_file))

    def print_trash_msg(self, trash_file):
        print("Moving original file ({0}) into Trash!".format(trash_file))

    def print_password_mismatch(self):
        print("The passwords entered did not match")


class CliRunner:
    def __init__(self, env_setup: EatConfig.EnvSetup, cli_int: CliInterface, passed_params):
        self.env_setup = env_setup
        self.cli_int = cli_int
        self.passed_params = passed_params
        self.target_name = passed_params["filename"]
        self.new_target_name = self._set_new_target_name()
        # self.par_targets = "{0}.*".format(self.new_target_name)
        # self.tar_par_file = "{0}.tpar".format(self.new_target_name)

    def cleanup_named_pipe(self):
        os.unlink(self.env_setup.read_rc["pass_pipe"])
        # command = "rm {0}".format(self.env_setup.read_rc["pass_pipe"])
        # subprocess.run(command)

    def _set_new_target_name(self) -> str:
        if not self.passed_params["gpg_detected"]:
            if self.passed_params["is_dir"]:
                if self.env_setup.read_rc["encryption_enabled"]:
                    return "{0}.tar.zst.gpg".format(self.target_name)
                else:
                    return "{0}.tar.zst".format(self.target_name)
            else:
                if self.env_setup.read_rc["encryption_enabled"]:
                    return "{0}.zst.gpg".format(self.target_name)
                else:
                    return "{0}.zst".format(self.target_name)
        else:
            return self.target_name[0:-4]

    def _create_named_pipe(self):
        pass_pipe = self.env_setup.read_rc["pass_pipe"]
        try:
            os.mkfifo(pass_pipe)
        except OSError as oe:
            if oe.errno != errno.EEXIST:
                raise
        # push_key_command = "echo {0} > {1} &".format(self._get_key(), pass_pipe)

        # def opener(pp):
        #     myfd = open(pp, "r")
        #
        # read_thread = threading.Thread(target=opener, args=(pass_pipe,), daemon=True)
        # read_thread.start()
        # fd = open(pass_pipe, "r")

        generated_key = self._get_key()

        def push_key_to_named_pipe(pipe_path: str):
            with open(pipe_path, "w") as named_pipe:
                named_pipe.write("{0}\n".format(generated_key))

        pipe_write_thread = threading.Thread(target=push_key_to_named_pipe, args=(pass_pipe,), daemon=True)
        pipe_write_thread.start()

        # with open(pass_pipe, "w") as named_pipe:
        #     named_pipe.write("{0}\n".format(generated_key))

        # with open(pass_pipe, "w") as outfile:
        #     subprocess.run(my_cmd, stdout=outfile)

        # named_pipe = open(pass_pipe, 'w')
        # named_pipe.write("{0}\n".format(self._get_key()))

        # named_pipe = os.open(pass_pipe, os.O_NONBLOCK)
        # os.write(named_pipe, self._get_key())

        # os.write(pass_pipe, "{0}\n".format(self._get_key()))

        # named_pipe.close()
        # subprocess.run(push_key_command)

    def _get_password(self, ) -> str:
        pass_one = "pass_one"
        pass_two = "pass_two"
        while pass_one != pass_two:
            pass_one = input("Password:")
            pass_two = input("Repeat Password:")
            if pass_one != pass_two:
                self.cli_int.print_password_mismatch()
        return pass_one

    def _get_key(self) -> str:
        hash_string_list = ["argon2", "LetsAddALittleSaltIntoTheMix", "-r", "-d", "-t", "3", "-m", "16", "-p", "2", "-l", "128"]
        pass_out = subprocess.run(["echo", self._get_password()], check=True, capture_output=True)
        key_out = subprocess.run(hash_string_list, input=pass_out.stdout, capture_output=True)
        key = key_out.stdout.decode('utf-8').strip()
        return key

    def _build_encrypt_file_command(self) -> list:
        self.cli_int.print_compress_msg(self.new_target_name)
        compress_string = "zstd {0}".format(self.env_setup.read_rc["compression_level"])
        encrypt_string = "gpg --symmetric --no-symkey-cache --s2k-mode=1 --cipher-algo {0}".format(
            self.env_setup.read_rc["cipher"])
        return [compress_string, "|", encrypt_string]

    def _build_decrypt_command(self) -> list:
        self.cli_int.print_decryption_message(self.target_name)
        password = input("Enter Password:")
        pass_command = "echo {0}".format(password)
        password = None
        hash_command = "argon2 LetsAddALittleSaltIntoTheMix -r -d -t 3 -m 16 -p 2 -l 128"
        decrypt_command = "gpg --batch --passphrase-fd 0 - -output {0} --decrypt {1}".format(self.new_target_name, self.target_name)
        return [pass_command, "|", hash_command, "|", decrypt_command]

    # def _build_encrypt_dir_command_old(self) -> list:
    #     self.cli_int.print_tar_and_compress_encrypt_msg(self.new_target_name)
    #     tar_string = "tar cf - {0}". format(self.target_name)
    #     compress_string = "zstd {0} {1}".format(self.env_setup.read_rc["compression_level"], self.env_setup.read_rc["threads"])
    #     encrypt_string = "gpg --symmetric --no-symkey-cache --s2k-mode=1 --cipher-algo {0}".format(self.env_setup.read_rc["cipher"])
    #     return [tar_string, "|", compress_string, "|",  encrypt_string]

    def _build_encrypt_dir_commands(self) -> list:
        self.cli_int.print_tar_and_compress_encrypt_msg(self.new_target_name)
        tar_command = ["tar", "cf", "-", self.target_name]
        compress_command = ["zstd", self.env_setup.read_rc["compression_level"], self.env_setup.read_rc["threads"]]
        encrypt_command = ["gpg", "--batch", "--passphrase-file",  self.env_setup.read_rc["pass_pipe"], "--output", self.new_target_name, "--symmetric", "--no-symkey-cache", "--s2k-mode=1", "--cipher-algo", self.env_setup.read_rc["cipher"]]
        return [tar_command, compress_command, encrypt_command]

    def _build_file_command(self) -> list:
        self.cli_int.print_compress_msg(self.new_target_name)
        return ["zstd", self.env_setup.read_rc["compression_level"], self.target_name]

    def _build_dir_command(self) -> list:
        self.cli_int.print_tar_and_compress_msg(self.new_target_name)
        command_string = "zstd {0} {1}".format(self.env_setup.read_rc["compression_level"], self.env_setup.read_rc["threads"])
        return ["tar", "-c", "-I", command_string, "-f", self.new_target_name, self.target_name]

    def _build_par_command(self) -> list:
        self.cli_int.print_par_msg(self.new_target_name)
        return ["par2", "c", "{0}".format(self.new_target_name)]

    # tar the final results for convenience
    # def _build_tar_par_command(self) -> list:
    #     self.cli_int.print_tar_par_msg(self.tar_par_file)
    #     return ["tar", "-cf", self.tar_par_file, self.par_targets]

    def _trash_old(self):
        return 0

    def run(self):
        if self.passed_params["gpg_detected"]:
            command = self._build_decrypt_command()
        elif self.passed_params["is_dir"]:
            if self.env_setup.read_rc["encryption_enabled"]:
                self._create_named_pipe()
                commands = self._build_encrypt_dir_commands()
                tar_out = subprocess.run(commands[0], check=True, capture_output=True)
                compress_out = subprocess.run(commands[1], input=tar_out.stdout, capture_output=True)
                encrypt_out = subprocess.run(commands[2], input=compress_out.stdout, capture_output=True)
            else:
                command = self._build_dir_command()
        else:
            if self.env_setup.read_rc["encryption_enabled"]:
                self._create_named_pipe()
                command = self._build_encrypt_file_command()
            else:
                command = self._build_file_command()
        if len(commands) > 0:
            # output_comp = subprocess.run(command)
            if "p" not in self.passed_params:
                output_par = subprocess.run(self._build_par_command())
                # output_tar_par = subprocess.run(self._build_tar_par_command())
        else:
            return None


class ArgsParser:
    def __init__(self, eat_config: EatConfig, cli_int: CliInterface):
        self.cli_int = cli_int
        self.parser = argparse.ArgumentParser(description=eat_config.DESCRIPTION)
        self.parser.add_argument("-p", "--skip-parchive", action="store_true", help="Skip parchive step")
        self.parser.add_argument("-c", "--skip-compress", action="store_true", help="Skip compression step")
        self.parser.add_argument("-e", "--encrypt", action="store_true", help="Encrypt the archive with gpg")
        self.parser.add_argument("-q", "--quiet", action="store_true", help="Quiet - suppress terminal output")
        self.parser.add_argument("-v", "--version", action="store_true", help="Show version info")
        self.parser.add_argument("filename", nargs="?")

    def parse(self):
        args = self.parser.parse_args()

        flag_params = {
            "is_dir": False,
            "gpg_detected": False
        }
        keys = ["skip_parchive", "skip_compress", "encrypt", "quiet", "version"]

        if args.version:
            cli_int.print_version()
            sys.exit(0)

        filename = args.filename
        if not exists(filename):
            raise FileNotFoundError
        else:
            flag_params["filename"] = filename[:-1] if filename.endswith("/") else filename  # strip trailing /
            if isdir(filename):
                flag_params["is_dir"] = True
            elif filename[-4] == ".gpg":
                flag_params["gpg_detected"] = True

        for k in keys:
            if args.__getattribute__(k):
                flag_params[k] = args.__getattribute__(k)

        return flag_params


if __name__ == "__main__":
    try:
        eat_config = EatConfig()
    except InvalidConfigFileException as e:
        print(e.message)
        sys.exit(1)

    cli_int = CliInterface(eat_config)

    try:
        arg_obj = ArgsParser(eat_config, cli_int)
        passed_params = arg_obj.parse()
        cli_runner = CliRunner(eat_config.env_setup, cli_int, passed_params)
        output = cli_runner.run()
    except KeyboardInterrupt:
        cli_runner.cleanup_named_pipe()
        print('Interrupted by user pressing Ctrl-c')
        try:
            sys.exit(130)
        except SystemExit:
            os._exit(130)

    cli_runner.cleanup_named_pipe()
    sys.exit(0)
