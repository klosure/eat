# *eat* - encryption (and) archive toolkit

![](logo.png)

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

## About
This is a Python based tool to compress (and in the near future encrypt files using GPG) and [Parchive](https://wiki.archlinux.org/title/Parchive). Supports multiple compression engines. The goal is to create a common easy to use interface that doesn't require me to remember a bunch of flags. This is the spiritual successor to my shell based app called [qe](https://gitlab.com/klosure/qe). The functionality from that tool has not been merged into this one yet. 

## Dependencies
+ Python 3.7.9 (or greater)
+ [gpg](https://www.gnupg.org/)
+ [argon2](https://github.com/P-H-C/phc-winner-argon2)
+ [par2](https://wiki.archlinux.org/title/Parchive)
+ [tar](https://www.unix.com/man-page/posix/1/tar/)
+ [zstd](https://github.com/facebook/zstd)


## Install
```bash
git clone
cd eat
sudo cp eat.py /usr/local/bin/eat
```

> Eat places an .eatrc file in the home directory to keep track of the default settings

## Usage
```bash
eat my_big_file.bin       # use default settings
eat -h                    # show help into
eat -v                    # show version info
eat -c my_big_file.bin    # Skips the compression step
eat -a my_big_file.bin    # Skips the Parchive step
eat -q my_big_file.bin    # Quiet - suppress terminal output
```
> *Note*: When you pass a directory, *eat* will tar it for you!

## TODO
+ check number of cores and set threads
+ Config file support
+ Before running main code check local deps
+ Add type hints
+ Run Black
+ Compression off flag
+ add [qe](https://gitlab.com/klosure/qe) functionality
+ Multiple files
+ Trash files flag (-t)
+ Quiet mode
+ write a man file
+ Add lz4/gzip support
+ Change to toml config instead of json after 3.11 is more widely installed