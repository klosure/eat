import unittest
import unittest.mock
import io


from eat import CliInterface, ArgsParser


class TestArgsParser(unittest.TestCase):
    cli_int_obj = CliInterface()
    ap_obj = ArgsParser(cli_int_obj)

    def test_happy_path(self):
        self.assertEqual(0, 1)


class TestCliRunner(unittest.TestCase):
    def test_happy_path(self):
        self.assertEqual(0, 1)


class TestCliInterface(unittest.TestCase):

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_version(self, mock_stdout):
        # Arrange
        version = "0.1"
        expected = "eat {0} - encryption and archive toolkit\n".format(version)
        cli_int_obj = CliInterface()

        # Act
        cli_int_obj.print_version()

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_tar_and_compress_msg(self, mock_stdout):
        # Arrange
        new_target_name = "newfile.tar.zst"
        expected = "Tar and compressing directory: {0}\n".format(new_target_name)
        cli_int_obj = CliInterface()

        # Act
        cli_int_obj.print_tar_and_compress_msg(new_target_name)

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_compress_msg(self, mock_stdout):
        # Arrange
        new_target_name = "newfile.zst"
        expected = "Compressing file: {0}\n".format(new_target_name)
        cli_int_obj = CliInterface()

        # Act
        cli_int_obj.print_compress_msg(new_target_name)

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_par_msg(self, mock_stdout):
        # Arrange
        new_target_name = "newfile.zst"
        expected = "Creating par files for: {0}\n".format(new_target_name)
        cli_int_obj = CliInterface()

         # Act
        cli_int_obj.print_par_msg(new_target_name)

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_tar_par_msg(self, mock_stdout):
        # Arrange
        target_name = "file.jpg"
        expected = "Tar all par files together into: {0}\n".format(target_name)
        cli_int_obj = CliInterface()

        # Act
        cli_int_obj.print_tar_par_msg(target_name)

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_trash_msg(self, mock_stdout):
        # Arrange
        trash_file = "oldfile.jpg"
        expected = "Moving original file ({0}) into Trash!\n".format(trash_file)
        cli_int_obj = CliInterface()

        # Act
        cli_int_obj.print_trash_msg(trash_file)

        # Assert
        self.assertEqual(mock_stdout.getvalue(), expected)


class TestEnvSetup(unittest.TestCase):
    def test_happy_path(self):
        self.assertEqual(0, 1)


class TestDepsChecker(unittest.TestCase):
    def test_happy_path(self):
        self.assertEqual(0, 1)